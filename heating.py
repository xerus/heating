#!/usr/bin/python

# Import required Python libraries
import RPi.GPIO as GPIO
import time

# Use BCM GPIO references instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# init lists with pin numbers
BOILER_OUT = 7
SOLAR_OUT = 8
SOLAR_IN = 17
thermostats = [27, 22]
inputPins = thermostats + [SOLAR_IN]
outputPins = [4, 18, 23, 24, 25, SOLAR_OUT, BOILER_OUT]


def pins_active(pins):
    return sum(map(lambda x: GPIO.input(x), pins))


# setup input pins
for i in inputPins:
    GPIO.setup(i, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# setup output pins
for i in outputPins:
    GPIO.setup(i, GPIO.OUT, initial=GPIO.HIGH)

try:
    while True:
        for i in range(len(thermostats)):
            GPIO.output(outputPins[i], not GPIO.input(thermostats[i]))
        activate = pins_active(thermostats)
        if activate:
            if not GPIO.input(SOLAR_IN):
                GPIO.output(BOILER_OUT, activate)
                GPIO.output(SOLAR_OUT, not activate)
            else:
                GPIO.output(BOILER_OUT, not activate)
                GPIO.output(SOLAR_OUT, activate)
        else:
            GPIO.output(BOILER_OUT, GPIO.HIGH)
            GPIO.output(SOLAR_OUT, GPIO.HIGH)

        time.sleep(600)

except KeyboardInterrupt:
    print("Quit")

finally:
    GPIO.cleanup()
