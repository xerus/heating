Heating control with Raspberry pi
#################################

.. image:: https://gitlab.com/xerus/heating/badges/master/build.svg
    :alt: Build status

A simple system for turning on/off a boiler and circulation circuit.
